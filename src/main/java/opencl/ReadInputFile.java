package opencl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadInputFile {

	static int line = 0;

	public static void main(String[] args) throws IOException {
		line = countLines("C:\\movielens\\ratings.dat");
		int lines = readFile("C:\\movielens\\ratings.dat", "::");
		System.out.print(lines);

	}

	public static int countLines(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

	public static int readFile(String filename, String delimiter) throws IOException {
		float[] input = new float[3952 * 6040];
		BufferedReader br = new BufferedReader(new FileReader(filename));
		int count = 0;
		String line;
		while ((line = br.readLine()) != null) {
			String[] x_value = line.split(delimiter); // You can parse string
														// array into int.
			count = x_value.length;
			int user = Integer.parseInt(x_value[0]);
			int movie = Integer.parseInt(x_value[1]);
			input[(user - 1) * 3952 + movie] = 1;
		}
		br.close();
		return count;
	}

	static String readFileToString(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static void listFilesForFolder(final String path) {
		final File folder = new File(path);
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				//listFilesForFolder(fileEntry.getAbsolutePath());
			} else {
				System.out.println(fileEntry.getName());
			}
		}
	}
}
