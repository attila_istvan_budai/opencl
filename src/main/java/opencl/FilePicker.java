package opencl;


import java.io.*;

import javax.swing.*;

	public class FilePicker extends JFrame {
		
		private String selectedFileName;
		
		Controller controller = null;  
		
		public FilePicker(Controller c)
		{
			controller = c;

		}
		
		public String getFileName()
		{
			return selectedFileName;
		}
	   
	   public void show()
	   {
		      SwingUtilities.invokeLater(new Runnable() {
	         public void run() {
	            createAndShowGui();
	         }
	      });
	   }

	   private void createAndShowGui() {
	      JFileChooser fileChooser = new JFileChooser();
	      int response = fileChooser.showOpenDialog(null);
	      if (response == JFileChooser.APPROVE_OPTION) {
	         File file = fileChooser.getSelectedFile();
	         selectedFileName = file.getAbsolutePath();
	         controller.showFrameB();
	         
	        
	      }
	   }
	   

	}