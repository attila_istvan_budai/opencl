package opencl;
import static org.bridj.Pointer.allocateFloats;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.bridj.Pointer;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.util.IOUtils;


public class RBM {
	
	int colNumber;
	int rowNumber;
	
	public void runRBM() throws IOException
	{
		float[] input = readFile("D:\\moviedata\\ratings.test", "::");
		System.out.println("input file loaded");
		System.out.println("rows: " + rowNumber + " cols: "+ colNumber);
		
		CLContext context = JavaCL.createBestContext();
		CLQueue queue = context.createDefaultQueue();
		ByteOrder byteOrder = context.getByteOrder();
		
		Pointer<Float> visiblePtr = allocateFloats(3952).order(byteOrder),
				hiddenPtr = allocateFloats(100).order(byteOrder),
				weightPtr =  allocateFloats(3952*6040),
				positiveGradientPtr = allocateFloats(3952*6040),
				negativeGradientPtr = allocateFloats(3952*6040);
		
		float [] subArray = Arrays.copyOfRange(input, 0, 3952);
		visiblePtr.setArray(subArray);
		
		CLBuffer<Float> visibleBuffer = context.createBuffer(Usage.Input, visiblePtr),
				hiddenBuffer = context.createBuffer(Usage.Input, hiddenPtr),
				weightBuffer = context.createBuffer(Usage.InputOutput, weightPtr),
				positiveGradientBuffer = context.createBuffer(Usage.Input, positiveGradientPtr),
				negativeGradient = context.createBuffer(Usage.Input, negativeGradientPtr);
		
		int[] global_size = new int[2];
		global_size[0] = 3952;
		global_size[1] = 100;
		
		// Read the program sources and compile them :
		String src;
		try {
			src = IOUtils.readText(getClass().getResource("rbm.cl"));
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("calculate_gradient");
			addFloatsKernel.setArgs(weightBuffer, visibleBuffer, hiddenBuffer, 100, 3952);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = weightBuffer.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 100 && i < 3952 * 100; i++)
				System.out.print(outPtr.get(i) + " ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
    public float[] readFile(String filename, String delimiter) throws IOException 
    {
    	float[] input = new float[3952*6040];
    	BufferedReader br = new BufferedReader(new FileReader(filename));
    	rowNumber =0;
    	String line;
    	String[] numbers = null;
    	while ((line = br.readLine()) != null) {
    		 numbers=line.split(delimiter); //You can parse string array into int.
    		 rowNumber++;
    		int user = Integer.parseInt(numbers[0]);
    		int movie = Integer.parseInt(numbers[1]);
    		input[(user-1)*3952+movie] = 1;
    	}
    	colNumber=numbers.length;
    	br.close();
    	return input;
    }
    
	   public static void main(String[] args) throws IOException {
		   RBM r = new RBM();
		   r.runRBM();
	   }

}
