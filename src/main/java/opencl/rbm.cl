

float sigmoid(float z)
{
	return 1.0/(1.0+exp(-z));
}


float sigmoid4(float4 z)
{
	return 1.0/(1.0+exp(-1*(z.x+z.y+z.z+z.w)));
}


__kernel void sigmoidKernel(float input, float output)
{
	output =  1.0/(1.0+exp(input));
}

__kernel void updateWeightsMiniBatch(global float* weight, global float* posgradient, global float* neggradient, const float alpha, const int size_of_batch)
{
	uint j = get_global_id(0);  
	weight[j] += (posgradient[j] - neggradient[j])*alpha/(float)size_of_batch;
}

__kernel void calculateHidden(global float* visible, global float* weight, global float* hidden,  const int hidden_number, const int visible_number)
{
    uint j = get_global_id(0);      
    float sum = 0; 
	global float* w = (global float*)(weight+j);
    for(int i=0; i<visible_number; i++)
    {
        sum += visible[i] * w[i*(hidden_number)];
    }
    hidden[j] = sigmoid(sum);
}

__kernel void calculateVisible(global float* hidden, global float* weight, global float* visible,  const int hidden_number, const int visible_number)
{
    uint j = get_global_id(0);      
    float sum = 0; 
	global float* w = (global float*)(weight+j*hidden_number);
    for(int i=0; i<hidden_number; i++)
    {
        sum += hidden[i] * w[i];
    }
    visible[j] =   sigmoid(sum);
}

__kernel void calculate_gradient(global float* gradient, global float* hidden, global float* visible,  const int hidden_number, const int visible_number)
{
	uint i = get_global_id(0); 
	uint j = get_global_id(1);  
	gradient[i*hidden_number+j] = visible[i]*hidden[j];
}

__kernel void update_weights(global float* weight, global float* posgradient, global float* neggradient, const float alpha)
{
	uint j = get_global_id(0);  
	weight[j] += (posgradient[j] - neggradient[j])*alpha;
}

__kernel void calculate_hidden_mini_batch(global float* visible, global float* weight, global float* hidden,  const int hidden_number, const int visible_number)
{
    uint j = get_global_id(0); 
	//batch 10 to 100
	uint k = get_global_id(1);      
    float sum = 0; 
	global float* w = (global float*)(weight+j);
	global float* v = (global float*)(visible+k*visible_number);
	global float* h = (global float*)(hidden+k*hidden_number);
    for(int i=0; i<visible_number; i++)
    {
        sum += v[i] * w[i*(hidden_number)];
    }
    h[j] = sigmoid(sum);
}

__kernel void calculate_visible_mini_batch(global float* hidden, global float* weight, global float* visible,  const int hidden_number, const int visible_number)
{
    uint j = get_global_id(0);
	//batch 10 to 100
	uint k = get_global_id(1);       
    float sum = 0; 
	global float* w = (global float*)(weight+j*hidden_number);
	global float* v = (global float*)(visible+k*visible_number);
	global float* h = (global float*)(hidden+k*hidden_number);
    for(int i=0; i<hidden_number; i++)
    {
        sum += h[i] * w[i];
    }
    v[j] =   sigmoid(sum);
}

__kernel void calculate_gradient_mini_batch(global float* gradient, global float* hidden, global float* visible,  const int hidden_number, const int visible_number, const int batchsize, local float* sumarray)
{
	uint i = get_global_id(0); 
	uint j = get_global_id(1);    
	uint k = get_local_id(2); 
	//uint l = get_local_id(1); 
	global float* v = (global float*)(visible+k*visible_number);
	global float* h = (global float*)(hidden+k*hidden_number);
	sumarray[k] = v[i]*h[j];
	//gradient[i*hidden_number+j] += v[i]*h[j];

	barrier(CLK_LOCAL_MEM_FENCE);
	if(k==0)
	{
		for(int n=0; n<batchsize; n++)
		{
			gradient[i*hidden_number+j] += sumarray[n];
		}
	} 
}




__kernel void CD(global float* input, global float* visible, global float* hidden, global float* weight,  const int hidden_number, const int visible_number, global float* posgradient, global float* neggradient, const float alpha)
{
	//v->h
	
    uint h_id = get_global_id(0);      
    float sum = 0; 
	global float* w = (global float*)(weight+h_id);
    for(int i=0; i<visible_number+1; i++)
    {
        sum += visible[i] * w[i*(hidden_number)];
    }
    hidden[h_id] = sigmoid(sum);

		uint v_id = get_global_id(1);   
	posgradient[v_id*hidden_number+h_id] = visible[v_id]*hidden[h_id];
	
	barrier(CLK_LOCAL_MEM_FENCE);
	
	//h->v'   
    sum = 0; 
	global float * w2 = (global float*)(weight+v_id*hidden_number);
    for(int i=0; i<hidden_number; i++)
    {
        sum += hidden[i] * w2[i];
    }
    visible[v_id] =   sigmoid(sum);

	barrier(CLK_LOCAL_MEM_FENCE);

	//v'->h'
	sum = 0; 
	w = (global float*)(weight+h_id);
    for(int i=0; i<visible_number+1; i++)
    {
        sum += visible[i] * w[i*(hidden_number)];
    }
    hidden[h_id] = sigmoid(sum);

	barrier(CLK_LOCAL_MEM_FENCE);

	neggradient[v_id*hidden_number+h_id] = visible[v_id]*hidden[h_id];

	barrier(CLK_LOCAL_MEM_FENCE);

	weight[v_id*hidden_number+h_id] += (posgradient[v_id*hidden_number+h_id] - neggradient[v_id*hidden_number+h_id])*alpha;
}


//==============================================================================================================
