package opencl;




public class Controller {
	private FilePicker filePicker;  
	private DataConfiguration frameb;  
	   // Constructor  
	   public Controller()  
	   {  
		   filePicker = new FilePicker(this);  
		   filePicker.setVisible(true);
	   }  
	   public void showFrameB()  
	   {  
		   filePicker.dispose();
		   frameb = new DataConfiguration(this, filePicker.getFileName());  
		   frameb.setVisible(true);
	   }  
	   
	   public static void main(String[] args) {
		   Controller c = new Controller();
	   }

}
