package opencl;
import javax.swing.JFrame;
import javax.swing.JTable;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

public class DataConfiguration extends JFrame {
	Controller controller = null;
	private JTable table;
	private JTextField textField;
	String filename;

	public DataConfiguration(Controller c, String fname) {
		controller = c;

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, screenSize.width, screenSize.height);

		this.filename = fname;
		DefaultTableModel model = new DefaultTableModel(0, 0);
		table = new JTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);

		JLabel lblDelimiter = new JLabel("Delimiter:");
		panel.add(lblDelimiter);

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);

		JButton btnRefresh = new JButton("Refresh");
		panel.add(btnRefresh);

		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					readFirstLines(filename, textField.getText(), table);
				} catch (IOException exp) {
					// TODO Auto-generated catch block
					exp.printStackTrace();
				}

			}
		});

		try {
			readFirstLines(filename, " ", table);
			table.repaint();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void readFirstLines(String filename, String delimiter,
			JTable MyJTable) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		DefaultTableModel model = (DefaultTableModel) MyJTable.getModel();
		model.setRowCount(0);
		int rownumber = 0;
		String line;
		while ((line = br.readLine()) != null && rownumber < 10) {
			rownumber++;
			String[] x_value = line.split(delimiter); // You can parse string
														// array into int.
			Vector row = new Vector();
			int columnNumber = x_value.length;
			for (String col : x_value) {
				row.add(col);
			}
			model.setColumnCount(columnNumber);
			model.addRow(row);
		}
		br.close();
	}
	
}
