package dnd;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;

public class DragFileDemo extends JPanel implements ActionListener {
	JTextArea fileArea;

	JFileChooser fc;

	JButton clear;
	private JTable table;
	private JTextField textField;

	TabbedPaneController tpc;

	public DragFileDemo() {
		super(new BorderLayout());


		
		JPanel upperPanel = new JPanel(new BorderLayout());
		upperPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setUpTable(upperPanel);


		clear = new JButton("Clear All");
		clear.addActionListener(this);
		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		buttonPanel.add(clear, BorderLayout.LINE_END);

		 upperPanel.add(buttonPanel, BorderLayout.PAGE_END);

		// The TabbedPaneController manages the panel that
		// contains the tabbed pane. When there are no files
		// the panel contains a plain text area. Then, as
		// files are dropped onto the area, the tabbed panel
		// replaces the file area.
		JTabbedPane tabbedPane = new JTabbedPane();
		JPanel tabPanel = new JPanel(new BorderLayout());
		tabPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		tpc = new TabbedPaneController(tabbedPane, tabPanel, table, textField);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, upperPanel, tabPanel);
		splitPane.setDividerLocation(400);
		splitPane.setPreferredSize(new Dimension(530, 650));
		add(splitPane, BorderLayout.CENTER);
	}
	
	public void setUpTable(JPanel upperPanel){
		JPanel panel = new JPanel();
		DefaultTableModel model = new DefaultTableModel(0, 0);
		table = new JTable(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		upperPanel.add(scrollPane, BorderLayout.CENTER);
		
		setUpTopPanel(panel);
		upperPanel.add(panel, BorderLayout.NORTH);
	}
	
	public void setUpTopPanel(JPanel panel){
		JLabel lblDelimiter = new JLabel("Delimiter:");
		panel.add(lblDelimiter);

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnRefresh = new JButton("Refresh");
		panel.add(btnRefresh);

		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tpc.refreshTable();

			}
		});
		
		JButton btnFilePicker = new JButton("Open File");
		panel.add(btnFilePicker);
		
		btnFilePicker.addActionListener(new OpenFilePicker());
	}

	public void setDefaultButton() {
		getRootPane().setDefaultButton(clear);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == clear) {
			tpc.clearAll();
		}
	}
	
	  class OpenFilePicker implements ActionListener {
		    public void actionPerformed(ActionEvent e) {
		      JFileChooser c = new JFileChooser();
		      // Demonstrate "Open" dialog:
		      int rVal = c.showOpenDialog(DragFileDemo.this);
		      if (rVal == JFileChooser.APPROVE_OPTION) {
		    	  try {
					tpc.filePicked(c.getSelectedFile());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//		        filename.setText(c.getSelectedFile().getName());
//		        dir.setText(c.getCurrentDirectory().toString());
		      }
		      if (rVal == JFileChooser.CANCEL_OPTION) {
//		        filename.setText("You pressed cancel");
//		        dir.setText("");
		      }
		    }
		  }

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	private static void createAndShowGUI() {
		// Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);

		// Create and set up the window.
		JFrame frame = new JFrame("DragFileDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the menu bar and content pane.
		DragFileDemo demo = new DragFileDemo();
		demo.setOpaque(true); // content panes must be opaque
		frame.setContentPane(demo);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
		demo.setDefaultButton();
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}