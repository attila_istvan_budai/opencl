package dnd;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.TransferHandler;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;

/*
 * FileAndTextTransferHandler.java is used by the 1.4 DragFileDemo.java example.
 */

class FileAndTextTransferHandler extends TransferHandler {
	private DataFlavor fileFlavor;

	private TabbedPaneController tpc;

	private JTextArea source;

	private boolean shouldRemove;

	protected String newline = "\n";

	private int currentrow;

	private File file;

	JTextArea tc;
	JTable table;
	JTextField textField;
	DefaultTableModel tableModel;

	// Start and end position in the source text.
	// We need this information when performing a MOVE
	// in order to remove the dragged text from the source.
	Position p0 = null, p1 = null;

	FileAndTextTransferHandler(TabbedPaneController t, JTable table, JTextField textField) {
		currentrow = 0;
		tpc = t;
		fileFlavor = DataFlavor.javaFileListFlavor;
		this.table = table;
		this.textField = textField;
	}

	@Override
	public boolean importData(JComponent c, Transferable t) {

		if (!canImport(c, t.getTransferDataFlavors())) {
			return false;
		}
		// A real application would load the file in another
		// thread in order to not block the UI. This step
		// was omitted here to simplify the code.
		try {
			if (hasFileFlavor(t.getTransferDataFlavors())) {

				List<File> files = (List<File>) t.getTransferData(fileFlavor);
				for (int i = 0; i < files.size(); i++) {
					readData(files.get(i));
				}
				return true;
			}
		} catch (UnsupportedFlavorException ufe) {
			System.out.println("importData: unsupported data flavor");
		} catch (IOException ieo) {
			System.out.println("importData: I/O exception");
		}
		return false;
	}
	
	public void readData(File file) throws IOException{
		this.file = file;
		tc = tpc.addTab(file.toString());
		readLineFromFileToTextArea(file, tc, 0, 10);
		readFirstLinesToTable(file, textField.getText(), table);
	}

	public void readNextRows() throws IOException {
		readLineFromFileToTextArea(file, tc, currentrow, currentrow + 10);
		System.out.println(currentrow);
	}

	public void readLineFromFileToTextArea(File file, JTextArea tc, int from, int to) throws IOException {
		currentrow = to;
		String str = null;
		BufferedReader in = null;

		in = new BufferedReader(new FileReader(file));
		int line = 0;

		while ((str = in.readLine()) != null && line < to) {
			if (line > from) {
				tc.append(str + newline);

			}
			line++;
		}
		in.close();
	}

	public void readFirstLinesToTable(File file, String delimiter, JTable MyJTable) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		setTableModel(MyJTable);
		int rownumber = 0;
		String line;
		while ((line = br.readLine()) != null && rownumber < 10) {
			rownumber++;
			addRowToTable(line, delimiter);
		}
		br.close();
	}
	
	public void refresTable(){
		try {
			readFirstLinesToTable(file, textField.getText(), table);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setTableModel(JTable MyJTable) {
		tableModel = (DefaultTableModel) MyJTable.getModel();
		tableModel.setRowCount(0);
	}

	private void addRowToTable(String line, String delimiter) {
		String[] x_value = line.split(delimiter); // You can parse string
		Vector row = new Vector();
		int columnNumber = x_value.length;
		for (String col : x_value) {
			row.add(col);
		}
		tableModel.setColumnCount(columnNumber);
		tableModel.addRow(row);
	}

	protected Transferable createTransferable(JComponent c) {
		source = (JTextArea) c;
		int start = source.getSelectionStart();
		int end = source.getSelectionEnd();
		Document doc = source.getDocument();
		if (start == end) {
			return null;
		}
		try {
			p0 = doc.createPosition(start);
			p1 = doc.createPosition(end);
		} catch (BadLocationException e) {
			System.out.println("Can't create position - unable to remove text from source.");
		}
		shouldRemove = true;
		String data = source.getSelectedText();
		return new StringSelection(data);
	}

	public int getSourceActions(JComponent c) {
		return COPY_OR_MOVE;
	}

	// Remove the old text if the action is a MOVE.
	// However, we do not allow dropping on top of the selected text,
	// so in that case do nothing.
	protected void exportDone(JComponent c, Transferable data, int action) {
		if (shouldRemove && (action == MOVE)) {
			if ((p0 != null) && (p1 != null) && (p0.getOffset() != p1.getOffset())) {
				try {
					JTextComponent tc = (JTextComponent) c;
					tc.getDocument().remove(p0.getOffset(), p1.getOffset() - p0.getOffset());
				} catch (BadLocationException e) {
					System.out.println("Can't remove text from source.");
				}
			}
		}
		source = null;
	}

	public boolean canImport(JComponent c, DataFlavor[] flavors) {
		if (hasFileFlavor(flavors)) {
			return true;
		}
		return false;
	}

	private boolean hasFileFlavor(DataFlavor[] flavors) {
		for (int i = 0; i < flavors.length; i++) {
			if (fileFlavor.equals(flavors[i])) {
				return true;
			}
		}
		return false;
	}

}