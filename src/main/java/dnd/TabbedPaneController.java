package dnd;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.TransferHandler;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
/*
 * TabbedPaneController.java is used by the 1.4 DragFileDemo.java example.
 */

/*
 * Class that manages area where the contents of files are displayed. When no
 * files are present, there is a simple JTextArea instructing users to drop a
 * file. As soon as a file is dropped, a JTabbedPane is placed into the window
 * and each file is displayed under its own tab. When all the files are removed,
 * the JTabbedPane is removed from the window and the simple JTextArea is again
 * displayed.
 */

class TabbedPaneController {
  JPanel tabbedPanel = null;

  JTabbedPane tabbedPane;

  JPanel emptyFilePanel = null;

  JTextArea emptyFileArea = null;
  
  private  JTable table = null;
  
  private JTextField textField = null;

  FileAndTextTransferHandler transferHandler;

  boolean noFiles = true;

  String fileSeparator;

  public TabbedPaneController(JTabbedPane tb, JPanel tp, JTable table, JTextField tf) {
    tabbedPane = tb;
    tabbedPanel = tp;
    this.table = table;
    this.textField = tf;
    transferHandler = new FileAndTextTransferHandler(this, table, tf);
    fileSeparator = System.getProperty("file.separator");
    if ("\\".equals(fileSeparator)) {
      fileSeparator = "\\\\";
    }
    init();
  }

  public JTextArea addTab(String filename) {
	  System.out.println(filename);
    if (noFiles) {
      tabbedPanel.remove(emptyFilePanel);
      tabbedPanel.add(tabbedPane, BorderLayout.CENTER);
      noFiles = false;
    }
    String[] str = filename.split(fileSeparator);
    return makeTextPanel(str[str.length - 1], filename);
  }

  //Remove all tabs and their components, then put the default
  //file area back.
  public void clearAll() {
    if (noFiles == false) {
      tabbedPane.removeAll();
      tabbedPanel.remove(tabbedPane);
    }
    init();
  }
  
  public void refreshTable(){
	  transferHandler.refresTable();
  }
  
  public void filePicked(File file) throws IOException{
	  transferHandler.readData(file);

  }

  private void init() {
    String defaultText = "Select one or more files from the file chooser and drop here...";
    noFiles = true;
    if (emptyFilePanel == null) {
      emptyFileArea = new JTextArea(20, 15);
      emptyFileArea.setEditable(false);
      emptyFileArea.setDragEnabled(true);
      emptyFileArea.setTransferHandler(transferHandler);
      emptyFileArea.setMargin(new Insets(5, 5, 5, 5));
      JScrollPane fileScrollPane = new JScrollPane(emptyFileArea);
      emptyFilePanel = new JPanel(new BorderLayout(), false);
      emptyFilePanel.add(fileScrollPane, BorderLayout.CENTER);
    }
    tabbedPanel.add(emptyFilePanel, BorderLayout.CENTER);
    tabbedPanel.repaint();
    emptyFileArea.setText(defaultText);
  }

  protected JTextArea makeTextPanel(String name, String toolTip) {
    JTextArea fileArea = new JTextArea(20, 15);
    fileArea.setDragEnabled(true);
    fileArea.setTransferHandler(transferHandler);
    fileArea.setMargin(new Insets(5, 5, 5, 5));
    JScrollPane fileScrollPane = new JScrollPane(fileArea);
    tabbedPane.addTab(name, null, (Component) fileScrollPane, toolTip);
    tabbedPane.setSelectedComponent((Component) fileScrollPane);
    fileScrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener(){
		public void adjustmentValueChanged(AdjustmentEvent ae) {
		       JScrollBar scrollBar = (JScrollBar) ae.getAdjustable();
		        int extent = scrollBar.getModel().getExtent();
		        System.out.println("1. Value: " + (scrollBar.getValue() + extent) + " Max: " + scrollBar.getMaximum());
		        if(scrollBar.getValue() + extent == scrollBar.getMaximum()){
		        	try {
						transferHandler.readNextRows();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
		}
	}
	);
    return fileArea;
  }
}