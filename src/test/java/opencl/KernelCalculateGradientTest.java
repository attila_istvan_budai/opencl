package opencl;
import static org.bridj.Pointer.allocateFloats;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.bridj.Pointer;
import org.junit.Before;
import org.junit.Test;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.util.IOUtils;

public class KernelCalculateGradientTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;

	@Before
	public void initObjects() {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
	}

	@Test
	public void TestNormalCase() {
		int visible = 1024;
		int hidden = 100;
		Pointer<Float> visiblePtr = allocateFloats(visible).order(byteOrder), hiddenPtr = allocateFloats(hidden).order(byteOrder);

		for (int i = 0; i < visible; i++) {
			visiblePtr.set(i, 1.0f);
		}

		for (int i = 0; i < hidden; i++) {
			hiddenPtr.set(i, 1.0f);
		}

		int[] global_size = new int[2];
		global_size[0] = visible;
		global_size[1] = hidden;

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, visiblePtr), c = context.createBuffer(Usage.Input, hiddenPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, visible * hidden);

		// Read the program sources and compile them :
		String src;
		try {
			ReadInputFile.listFilesForFolder("./src/main/java/opencl/");
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("calculate_gradient");
			addFloatsKernel.setArgs(out, b, c, hidden, visible);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < visible * hidden; i++)
				assertEquals("", new Float(1.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void TestWithTwoNumbers() {
		int visible = 1024;
		int hidden = 100;
		Pointer<Float> visiblePtr = allocateFloats(visible).order(byteOrder), hiddenPtr = allocateFloats(hidden).order(byteOrder);

		for (int i = 0; i < visible; i++) {
			visiblePtr.set(i, 2.0f);
		}

		for (int i = 0; i < hidden; i++) {
			hiddenPtr.set(i, 2.0f);
		}

		int[] global_size = new int[2];
		global_size[0] = visible;
		global_size[1] = hidden;

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, visiblePtr), c = context.createBuffer(Usage.Input, hiddenPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, visible * hidden);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("calculate_gradient");
			addFloatsKernel.setArgs(out, b, c, hidden, visible);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < visible * hidden; i++)
				assertEquals("", new Float(4.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void TestWithZero() {
		int visible = 1024;
		int hidden = 100;
		Pointer<Float> visiblePtr = allocateFloats(visible).order(byteOrder), hiddenPtr = allocateFloats(hidden).order(byteOrder);

		for (int i = 0; i < visible; i++) {
			visiblePtr.set(i, 0.0f);
		}

		for (int i = 0; i < hidden; i++) {
			hiddenPtr.set(i, 2.0f);
		}

		int[] global_size = new int[2];
		global_size[0] = visible;
		global_size[1] = hidden;

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, visiblePtr), c = context.createBuffer(Usage.Input, hiddenPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.InputOutput, visible * hidden);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("calculate_gradient");
			addFloatsKernel.setArgs(out, b, c, hidden, visible);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, global_size);

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < visible * hidden; i++)
				assertEquals("", new Float(0.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
