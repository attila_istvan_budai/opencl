package opencl;



import static org.bridj.Pointer.allocateFloats;
import static org.junit.Assert.*; // Allows you to use directly assert methods, such as assertTrue(...), assertNull(...)

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.bridj.Pointer;
import org.junit.Test; // for @Test
import org.junit.Before; // for @Before

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.util.IOUtils;

//__kernel void update_weights(global float* weight, global float* posgradient, global float* neggradient, const float alpha)
public class KernelUpdateWeightsTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;

	@Before
	public void initObjects() {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
	}

	@Test
	public void TestNormalCase() {
		int n = 1024;
		Pointer<Float> bPtr = allocateFloats(n).order(byteOrder), cPtr = allocateFloats(
				n).order(byteOrder);

		for (int i = 0; i < n; i++) {
			bPtr.set(i, 2.0f);
			cPtr.set(i, 1.0f);
		}

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, bPtr), c = context
				.createBuffer(Usage.Input, cPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(
				CLMem.Usage.InputOutput, n);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("update_weights");
			addFloatsKernel.setArgs(out, b, c, 1f);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue,
					new int[] { n });

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < n; i++)
				assertEquals("", new Float(1.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void TestWithZeroAlpha() {
		int n = 1024;
		Pointer<Float> bPtr = allocateFloats(n).order(byteOrder), cPtr = allocateFloats(
				n).order(byteOrder);

		for (int i = 0; i < n; i++) {
			bPtr.set(i, 2.0f);
			cPtr.set(i, 1.0f);
		}

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, bPtr), c = context
				.createBuffer(Usage.Input, cPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(
				CLMem.Usage.InputOutput, n);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("update_weights");
			addFloatsKernel.setArgs(out, b, c, 0f);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue,
					new int[] { n });

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < n; i++)
				assertEquals("", new Float(0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void TestWithAlphaEqualsTwo() {
		int n = 1024;
		Pointer<Float> bPtr = allocateFloats(n).order(byteOrder), cPtr = allocateFloats(
				n).order(byteOrder);

		for (int i = 0; i < n; i++) {
			bPtr.set(i, 2.0f);
			cPtr.set(i, 1.0f);
		}

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, bPtr), c = context
				.createBuffer(Usage.Input, cPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(
				CLMem.Usage.InputOutput, n);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("update_weights");
			addFloatsKernel.setArgs(out, b, c, 2f);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue,
					new int[] { n });

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < n; i++)
				assertEquals("", new Float(2.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void TestWithSameMatrixes() {
		int n = 1024;
		Pointer<Float> bPtr = allocateFloats(n).order(byteOrder), cPtr = allocateFloats(
				n).order(byteOrder);

		for (int i = 0; i < n; i++) {
			bPtr.set(i, 2.0f);
			cPtr.set(i, 2.0f);
		}

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> b = context.createBuffer(Usage.Input, bPtr), c = context
				.createBuffer(Usage.Input, cPtr);

		// Create an OpenCL output buffer :
		CLBuffer<Float> out = context.createFloatBuffer(
				CLMem.Usage.InputOutput, n);

		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("update_weights");
			addFloatsKernel.setArgs(out, b, c, 2f);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue,
					new int[] { n });

			Pointer<Float> outPtr = out.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < n; i++)
				assertEquals("", new Float(0.0f), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
