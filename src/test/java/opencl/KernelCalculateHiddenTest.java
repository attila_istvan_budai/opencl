package opencl;
import static org.bridj.Pointer.allocateFloats;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.bridj.Pointer;
import org.junit.Before;
import org.junit.Test;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.util.IOUtils;


//__kernel void calculate_hidden(global float* visible, global float* weight, global float* hidden,  const int hidden_number, const int visible_number)
public class KernelCalculateHiddenTest {
	CLContext context;
	CLQueue queue;
	ByteOrder byteOrder;

	@Before
	public void initObjects() {
		context = JavaCL.createBestContext();
		queue = context.createDefaultQueue();
		byteOrder = context.getByteOrder();
	}
	
	Float sigmoid(float z)
	{
		return (float) (1.0/(1.0+Math.exp(-z)));
	}


	@Test
	public void TestNormalCase() {
		int visible = 10;
		int hidden = 100;
		Pointer<Float> visiblePtr = allocateFloats(visible).order(byteOrder), hiddenPtr = allocateFloats(
				hidden).order(byteOrder), weightPtr = allocateFloats(visible*hidden).order(byteOrder);

		for (int i = 0; i < visible; i++) {
			visiblePtr.set(i, 2.0f);
		}
		
		for (int i = 0; i < hidden; i++) {
			hiddenPtr.set(i, 1.0f);
		}
		
		for (int i = 0; i < hidden*visible; i++) {
			weightPtr.set(i, 1.0f);
		}
		
		int[] global_size = new int [1];
		global_size[0] = hidden;
		

		// Create OpenCL input buffers (using the native memory pointers aPtr
		// and bPtr) :
		CLBuffer<Float> visibleBuffer = context.createBuffer(Usage.Input, visiblePtr), hiddenBuffer = context
				.createBuffer(Usage.InputOutput, hiddenPtr), weightBuffer = context.createBuffer(Usage.Input, weightPtr);


		// Read the program sources and compile them :
		String src;
		try {
			src = ReadInputFile.readFileToString("./src/main/java/opencl/rbm.cl", Charset.defaultCharset());
			CLProgram program = context.createProgram(src);

			// Get and call the kernel :
			CLKernel addFloatsKernel = program.createKernel("calculateHidden");
			addFloatsKernel.setArgs(visibleBuffer, weightBuffer, hiddenBuffer, hidden, visible);
			CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue,
					global_size);

			Pointer<Float> outPtr = hiddenBuffer.read(queue, addEvt); // blocks until
																// add_floats
																// finished

			// Print the first 10 output values :
			for (int i = 0; i < 10 && i < visible*hidden; i++)
				assertEquals("", sigmoid(20), outPtr.get(i));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
